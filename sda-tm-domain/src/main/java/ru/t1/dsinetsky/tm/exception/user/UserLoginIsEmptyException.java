package ru.t1.dsinetsky.tm.exception.user;

public final class UserLoginIsEmptyException extends GeneralUserException {

    public UserLoginIsEmptyException() {
        super("User login cannot be empty!");
    }

}
