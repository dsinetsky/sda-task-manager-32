package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.data.*;
import ru.t1.dsinetsky.tm.dto.response.data.*;

public interface IDomainEndpoint {

    @NotNull
    DataBackupSaveResponse saveDataBackup(final DataBackupSaveRequest request);

    @NotNull
    DataBackupLoadResponse loadDataBackup(final DataBackupLoadRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(final DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(final DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadDataBinary(final DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(final DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(final DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(final DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxB(final DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxB(final DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(final DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(final DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxB(final DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxB(final DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(final DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(final DataYamlSaveFasterXmlRequest request);

}
