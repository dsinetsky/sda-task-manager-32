package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.system.SystemVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    SystemAboutResponse getAbout(@NotNull final SystemAboutRequest request);

    @NotNull
    SystemVersionResponse getVersion(@NotNull final SystemVersionRequest request);

}
