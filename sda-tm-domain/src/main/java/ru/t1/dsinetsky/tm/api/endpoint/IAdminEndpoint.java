package ru.t1.dsinetsky.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserLockResponse;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserUnlockResponse;

public interface IAdminEndpoint {
    @NotNull
    @SneakyThrows
    UserLockResponse lockUserByLogin(final UserLockRequest request);

    @NotNull
    @SneakyThrows
    UserUnlockResponse unlockUserByLogin(final UserUnlockRequest request);
}
