package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.NoArgsConstructor;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {
}
