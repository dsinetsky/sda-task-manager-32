package ru.t1.dsinetsky.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.response.AbstractResponse;
import ru.t1.dsinetsky.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectRemoveByIdResponse extends AbstractResponse {

    @Nullable
    private Project project;

}
