package ru.t1.dsinetsky.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserLoginResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthEndpoint {


    @NotNull
    @SneakyThrows
    UserLoginResponse login(final UserLoginRequest request);

    @NotNull
    @SneakyThrows
    UserLogoutResponse logout(final UserLogoutRequest request);

    @NotNull
    @SneakyThrows
    UserViewProfileResponse viewProfile(final UserViewProfileRequest request);

}
