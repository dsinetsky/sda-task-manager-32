package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.task.*;
import ru.t1.dsinetsky.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(final TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(final TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(final TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clearTask(final TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeTaskById(final TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeTaskByIndex(final TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse createTask(final TaskCreateRequest request);

    @NotNull
    TaskFindByIdResponse findTaskById(final TaskFindByIdRequest request);

    @NotNull
    TaskFindByIndexResponse findTaskByIndex(final TaskFindByIndexRequest request);

    @NotNull
    TaskListByProjectResponse listTaskByProject(final TaskListByProjectRequest request);

    @NotNull
    TaskListResponse listTask(final TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeTaskById(final TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(final TaskRemoveByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startTaskById(final TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startTaskByIndex(final TaskStartByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(final TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateTaskById(final TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(final TaskUpdateByIndexRequest request);

}
