package ru.t1.dsinetsky.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.response.AbstractResponse;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskListByProjectResponse extends AbstractResponse {

    @NotNull
    private List<Task> tasks;

}
