package ru.t1.dsinetsky.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;
import ru.t1.dsinetsky.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    private int index;

    @Nullable
    private Status status;

}
