package ru.t1.dsinetsky.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(final @NotNull Throwable throwable) {
        super(throwable);
    }

}
