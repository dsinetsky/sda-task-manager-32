package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IConnectEndpoint {

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

}
