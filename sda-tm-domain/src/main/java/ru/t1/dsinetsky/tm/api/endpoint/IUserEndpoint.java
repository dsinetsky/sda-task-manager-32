package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserChangePasswordResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateEmailResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateNameResponse;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(final UserChangePasswordRequest request);

    @NotNull
    UserRegistryResponse registryUser(final UserRegistryRequest request);

    @NotNull
    UserUpdateEmailResponse updateUserEmail(final UserUpdateEmailRequest request);

    @NotNull
    UserUpdateNameResponse updateUserName(final UserUpdateNameRequest request);

}
