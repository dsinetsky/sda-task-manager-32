package ru.t1.dsinetsky.tm.exception.user;

public final class UserIdIsEmptyException extends GeneralUserException {

    public UserIdIsEmptyException() {
        super("User Id cannot be empty!");
    }

}
