package ru.t1.dsinetsky.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

}
