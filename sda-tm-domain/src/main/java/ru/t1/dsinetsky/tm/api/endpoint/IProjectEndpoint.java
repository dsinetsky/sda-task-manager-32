package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.project.*;
import ru.t1.dsinetsky.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(final ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(final ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProject(final ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeProjectById(final ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeProjectByIndex(final ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse createProject(final ProjectCreateRequest request);

    @NotNull
    ProjectFindByIdResponse findProjectById(final ProjectFindByIdRequest request);

    @NotNull
    ProjectFindByIndexResponse findProjectByIndex(final ProjectFindByIndexRequest request);

    @NotNull
    ProjectListResponse listProject(final ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(final ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(final ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startProjectById(final ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startProjectByIndex(final ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(final ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(final ProjectUpdateByIndexRequest request);

}
