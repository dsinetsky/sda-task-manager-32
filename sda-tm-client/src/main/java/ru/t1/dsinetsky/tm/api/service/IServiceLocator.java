package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    DomainEndpointClient getDomainEndpoint();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    SystemEndpointClient getSystemEndpoint();

    @NotNull
    AdminEndpointClient getAdminEndpoint();

    @NotNull
    ConnectionEndpointClient getConnectionEndpoint();

}
