package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
