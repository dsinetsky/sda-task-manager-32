package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectFindByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectFindByIdResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectFindByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FIND_PROJECT_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Show project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectFindByIdRequest request = new ProjectFindByIdRequest();
        request.setId(id);
        @NotNull final ProjectFindByIdResponse response = getProjectEndpoint().findProjectById(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
