package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemAboutResponse;

public final class AboutDisplayCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_ABOUT;

    @NotNull
    public static final String NAME = TerminalConst.CMD_ABOUT;

    @NotNull
    public static final String DESCRIPTION = "Shows information about developer";

    @Override
    public void execute() {
        @NotNull final SystemAboutResponse response = getSystemEndpoint().getAbout(new SystemAboutRequest());
        System.out.println(response.getName());
        System.out.println(response.getEmail());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
