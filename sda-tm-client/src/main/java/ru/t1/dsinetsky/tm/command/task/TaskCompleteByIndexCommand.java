package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskCompleteByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskCompleteByIndexResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_COMPLETE_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Completes task (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest();
        request.setIndex(index);
        @NotNull final TaskCompleteByIndexResponse response = getTaskEndpoint().completeTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
        System.out.println("Task successfully completed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
