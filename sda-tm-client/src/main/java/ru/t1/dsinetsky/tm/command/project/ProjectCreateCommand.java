package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_CREATE;

    @NotNull
    public static final String DESCRIPTION = "Create new project";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter name of new project:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setName(name);
        request.setDescription(description);
        @NotNull final ProjectCreateResponse response = getProjectEndpoint().createProject(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
        System.out.println("Project successfully created!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
