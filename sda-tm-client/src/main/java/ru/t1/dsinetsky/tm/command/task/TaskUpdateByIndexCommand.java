package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskUpdateByIndexResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPD_TASK_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Updates name and description of task (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new name:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        @NotNull final TaskUpdateByIndexResponse response = getTaskEndpoint().updateTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
        System.out.println("Task successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
