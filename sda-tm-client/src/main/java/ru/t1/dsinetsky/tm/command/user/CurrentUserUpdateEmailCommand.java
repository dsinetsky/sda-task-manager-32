package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class CurrentUserUpdateEmailCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPDATE_CURRENT_USER_EMAIL;

    @NotNull
    public static final String DESCRIPTION = "Updates email of current user";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter new email:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserUpdateEmailRequest request = new UserUpdateEmailRequest(email);
        getUserEndpoint().updateUserEmail(request);
        System.out.println("User successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
