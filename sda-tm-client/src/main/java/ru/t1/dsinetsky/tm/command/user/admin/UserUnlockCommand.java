package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UNLOCK_USER;

    @NotNull
    public static final String DESCRIPTION = "Unlocks user to allow his login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(login);
        getAdminEndpoint().unlockUserByLogin(request);
        System.out.println("User successfully unlocked!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
