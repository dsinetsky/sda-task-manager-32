package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataJsonLoadFasterXmlRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_JSON_LOAD;

    @NotNull
    public static final String DESCRIPTION = "Loads data from json file with FasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonLoadFasterXmlRequest());
        System.out.println("File successfully loaded!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
