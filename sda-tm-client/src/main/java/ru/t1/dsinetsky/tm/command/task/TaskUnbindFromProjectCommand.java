package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UNBIND_TASK_TO_PROJECT;

    @NotNull
    public static final String DESCRIPTION = "Unbinds task to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        @Nullable final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        @Nullable final String taskEnter = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setTaskId(taskEnter);
        request.setProjectId(projectEnter);
        @NotNull final TaskUnbindFromProjectResponse response = getTaskEndpoint().unbindTaskFromProject(request);
        System.out.println("Task successfully unbound to project!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
