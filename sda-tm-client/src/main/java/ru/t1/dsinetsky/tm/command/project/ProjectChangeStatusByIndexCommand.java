package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectChangeStatusByIndexResponse;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Changes status of project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        @Nullable final String newStatus = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(newStatus);
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest();
        request.setIndex(index);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIndexResponse response = getProjectEndpoint().changeProjectStatusByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
        System.out.println("Status successfully changed");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
