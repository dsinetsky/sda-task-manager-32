package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectListRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectListResponse;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_LIST;

    @NotNull
    public static final String DESCRIPTION = "Show available projects";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest();
        request.setSort(sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @NotNull final List<Project> listProjects = response.getProjects();
        System.out.println("List of projects:");
        int index = 1;
        for (@NotNull final Project project : listProjects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
