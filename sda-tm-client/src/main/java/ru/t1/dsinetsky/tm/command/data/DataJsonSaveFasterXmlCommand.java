package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataJsonSaveFasterXmlRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_JSON_SAVE;

    @NotNull
    public static final String DESCRIPTION = "Saves data to json file with fasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonSaveFasterXmlRequest());
        System.out.println("File successfully saved!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
