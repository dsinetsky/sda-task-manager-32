package ru.t1.dsinetsky.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.system.SystemVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public SystemAboutResponse getAbout(@NotNull final SystemAboutRequest request) {
        return call(request, SystemAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SystemVersionResponse getVersion(@NotNull final SystemVersionRequest request) {
        return call(request, SystemVersionResponse.class);
    }

}
