package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;

import java.util.Collection;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    protected ISystemEndpoint getSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    protected void listCommands(@NotNull final Collection<AbstractCommand> commands) {
        for (@NotNull final ICommand command : commands) {
            System.out.println(command);
        }
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
