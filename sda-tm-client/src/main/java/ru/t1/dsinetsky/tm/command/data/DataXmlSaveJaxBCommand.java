package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataXmlSaveJaxBRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_JAXB_XML_SAVE;

    @NotNull
    public static final String DESCRIPTION = "Saves data to xml file with JaxB";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest());
        System.out.println("File successfully saved!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
