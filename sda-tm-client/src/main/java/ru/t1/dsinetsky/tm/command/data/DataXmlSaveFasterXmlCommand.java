package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataXmlSaveFasterXmlRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_XML_SAVE;

    @NotNull
    public static final String DESCRIPTION = "Saves data to xml file with FasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest());
        System.out.println("File successfully saved!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
