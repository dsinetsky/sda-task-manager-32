package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_REMOVE_TASK_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Removes task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(id);
        getTaskEndpoint().removeTaskById(request);
        System.out.println("Task successfully removed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
