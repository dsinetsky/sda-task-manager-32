package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataBase64LoadRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_BASE_LOAD;

    @NotNull
    public static final String DESCRIPTION = "Loads data from base64 file";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest());
        System.out.println("File successfully loaded!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
