package ru.t1.dsinetsky.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IConnectEndpoint;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.net.Socket;

public final class ConnectServerCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_CONNECT_SERVER;

    @NotNull
    public static final String DESCRIPTION = "Connect to server";

    @Override
    public void execute() throws GeneralException {
        try {
            @NotNull final IConnectEndpoint endpointClient = getServiceLocator().getConnectionEndpoint();
            @Nullable final Socket socket = endpointClient.connect();

            getServiceLocator().getAuthEndpoint().setSocket(socket);
            getServiceLocator().getSystemEndpoint().setSocket(socket);
            getServiceLocator().getDomainEndpoint().setSocket(socket);
            getServiceLocator().getProjectEndpoint().setSocket(socket);
            getServiceLocator().getTaskEndpoint().setSocket(socket);
            getServiceLocator().getUserEndpoint().setSocket(socket);
            getServiceLocator().getAdminEndpoint().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
