package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataBinaryLoadRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_BINARY_LOAD;

    @NotNull
    public static final String DESCRIPTION = "Loads data from binary file";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest());
        System.out.println("File successfully loaded!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
