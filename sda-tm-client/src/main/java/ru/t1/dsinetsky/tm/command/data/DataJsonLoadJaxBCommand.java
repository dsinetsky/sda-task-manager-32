package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataJsonLoadJaxBRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_JAXB_JSON_LOAD;

    @NotNull
    public static final String DESCRIPTION = "Loads data from json file with JaxB";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().loadDataJsonJaxB(new DataJsonLoadJaxBRequest());
        System.out.println("File successfully loaded!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
