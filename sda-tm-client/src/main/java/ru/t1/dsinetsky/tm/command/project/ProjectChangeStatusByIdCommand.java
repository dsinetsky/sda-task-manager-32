package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectChangeStatusByIdResponse;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Changes status of project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        @Nullable final String newStatus = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(newStatus);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setId(id);
        request.setStatus(status);
        @NotNull final ProjectChangeStatusByIdResponse response = getProjectEndpoint().changeProjectStatusById(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
        System.out.println("Status successfully changed");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
