package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectUpdateByIdResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPD_PROJECT_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Updates name and description of project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter new name:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest();
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        @NotNull final ProjectUpdateByIdResponse response = getProjectEndpoint().updateProjectById(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
        System.out.println("Project successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
