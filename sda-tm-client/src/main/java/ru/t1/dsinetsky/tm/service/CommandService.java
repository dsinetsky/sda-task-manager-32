package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.command.data.AbstractDataCommand;
import ru.t1.dsinetsky.tm.command.project.AbstractProjectCommand;
import ru.t1.dsinetsky.tm.command.system.AbstractSystemCommand;
import ru.t1.dsinetsky.tm.command.task.AbstractTaskCommand;
import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;

import java.util.ArrayList;
import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getProjectCommands() {
        @NotNull final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (@NotNull final AbstractCommand command : getTerminalCommands()) {
            if (!command.getName().isEmpty() && command instanceof AbstractProjectCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTaskCommands() {
        @NotNull final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (@NotNull final AbstractCommand command : getTerminalCommands()) {
            if (!command.getName().isEmpty() && command instanceof AbstractTaskCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getDataCommands() {
        @NotNull final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (@NotNull final AbstractCommand command : getTerminalCommands()) {
            if (!command.getName().isEmpty() && command instanceof AbstractDataCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getUserCommands() {
        @NotNull final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (@NotNull final AbstractCommand command : getTerminalCommands()) {
            if (!command.getName().isEmpty() && command instanceof AbstractUserCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getSystemCommands() {
        @NotNull final Collection<AbstractCommand> resultCollection = new ArrayList<>();
        for (@NotNull final AbstractCommand command : getTerminalCommands()) {
            if (!command.getName().isEmpty() && command instanceof AbstractSystemCommand)
                resultCollection.add(command);
        }
        return resultCollection;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return commandRepository.getArgumentCommands();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String commandName) throws InvalidCommandException {
        if (commandName == null || commandName.isEmpty()) throw new InvalidCommandException();
        @Nullable final AbstractCommand command = commandRepository.getCommandByName(commandName);
        if (command == null) throw new InvalidCommandException(commandName);
        return command;
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) throws InvalidArgumentException {
        if (argument == null || argument.isEmpty()) throw new InvalidArgumentException();
        @Nullable final AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new InvalidArgumentException(argument);
        return command;
    }

}
