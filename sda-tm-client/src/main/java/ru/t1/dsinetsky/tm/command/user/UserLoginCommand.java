package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_LOGIN_USER;

    @NotNull
    public static final String DESCRIPTION = "Logins user to system";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        getAuthEndpoint().login(request);
        System.out.println("Welcome, " + login + "!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
