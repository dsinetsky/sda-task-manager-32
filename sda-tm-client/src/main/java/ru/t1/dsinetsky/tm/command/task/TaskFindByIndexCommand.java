package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskFindByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskFindByIndexResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskFindByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FIND_TASK_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Show task (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final TaskFindByIndexRequest request = new TaskFindByIndexRequest(index);
        @NotNull final TaskFindByIndexResponse response = getTaskEndpoint().findTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
