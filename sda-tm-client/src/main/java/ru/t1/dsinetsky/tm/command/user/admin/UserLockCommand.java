package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_LOCK_USER;

    @NotNull
    public static final String DESCRIPTION = "Locks user to prevent his login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(login);
        getAdminEndpoint().lockUserByLogin(request);
        System.out.println("User successfully locked!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
