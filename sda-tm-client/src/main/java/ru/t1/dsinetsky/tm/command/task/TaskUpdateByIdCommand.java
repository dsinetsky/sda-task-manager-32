package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskUpdateByIdResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPD_TASK_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Updates name and description of task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter new name:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest();
        request.setId(id);
        request.setName(name);
        request.setDescription(description);
        @NotNull final TaskUpdateByIdResponse response = getTaskEndpoint().updateTaskById(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
        System.out.println("Task successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
