package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectCompleteByIndexResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_COMPLETE_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Completes project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest();
        request.setIndex(index);
        @NotNull final ProjectCompleteByIndexResponse response = getProjectEndpoint().completeProjectByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
        System.out.println("Project successfully completed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
