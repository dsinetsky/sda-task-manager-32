package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataJsonSaveJaxBRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_JAXB_JSON_SAVE;

    @NotNull
    public static final String DESCRIPTION = "Saves data to json file with JaxB";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().saveDataJsonJaxB(new DataJsonSaveJaxBRequest());
        System.out.println("File successfully saved!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
