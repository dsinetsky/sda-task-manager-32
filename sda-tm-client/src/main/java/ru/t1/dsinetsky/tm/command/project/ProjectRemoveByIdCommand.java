package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_REMOVE_PROJECT_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Removes project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(id);
        getProjectEndpoint().removeProjectById(request);
        System.out.println("Project successfully removed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
