package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_BIND_TASK_TO_PROJECT;

    @NotNull
    public static final String DESCRIPTION = "Binds task to project(if any)";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter Id of project:");
        @Nullable final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        @Nullable final String taskEnter = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest();
        request.setTaskId(taskEnter);
        request.setProjectId(projectEnter);
        getTaskEndpoint().bindTaskToProject(request);
        System.out.println("Task successfully bound to project!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
