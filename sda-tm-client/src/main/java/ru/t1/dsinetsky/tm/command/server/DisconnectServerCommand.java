package ru.t1.dsinetsky.tm.command.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DisconnectServerCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_DISCONNECT_SERVER;

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @Override
    public void execute() throws GeneralException {
        try {
            getServiceLocator().getConnectionEndpoint().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }

    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
