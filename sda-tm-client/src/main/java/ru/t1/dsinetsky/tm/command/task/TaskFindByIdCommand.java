package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskFindByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskFindByIdResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskFindByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FIND_TASK_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Show task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskFindByIdRequest request = new TaskFindByIdRequest(id);
        @NotNull final TaskFindByIdResponse response = getTaskEndpoint().findTaskById(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
