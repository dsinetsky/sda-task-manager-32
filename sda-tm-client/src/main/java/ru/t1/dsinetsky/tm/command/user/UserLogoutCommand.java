package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_LOGOUT_USER;

    @NotNull
    public static final String DESCRIPTION = "Logouts user from system";

    @Override
    public void execute() throws GeneralException {
        getAuthEndpoint().logout(new UserLogoutRequest());
        System.out.println("User logged out.");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
