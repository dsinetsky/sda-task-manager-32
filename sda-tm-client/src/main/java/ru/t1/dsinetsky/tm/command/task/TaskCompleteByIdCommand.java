package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskCompleteByIdResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_COMPLETE_ID;

    @NotNull
    public static final String DESCRIPTION = "Completes task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(id);
        @NotNull final TaskCompleteByIdResponse response = getTaskEndpoint().completeTaskById(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
        System.out.println("Task successfully completed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
