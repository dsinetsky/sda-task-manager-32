package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectClearRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_CLEAR;

    @NotNull
    public static final String DESCRIPTION = "Clear all projects";

    @Override
    public void execute() throws GeneralException {
        getProjectEndpoint().clearProject(new ProjectClearRequest());
        System.out.println("All projects successfully cleared!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
