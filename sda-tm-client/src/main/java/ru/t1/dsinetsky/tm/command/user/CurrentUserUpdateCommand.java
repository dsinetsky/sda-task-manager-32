package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class CurrentUserUpdateCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPDATE_CURRENT_USER;

    @NotNull
    public static final String DESCRIPTION = "Update name of current user";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter new first name:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter new last name:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter new middle name:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateNameRequest request = new UserUpdateNameRequest();
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        getUserEndpoint().updateUserName(request);
        System.out.println("User successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
