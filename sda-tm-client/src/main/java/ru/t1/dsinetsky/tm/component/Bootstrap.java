package ru.t1.dsinetsky.tm.component;

import javassist.Modifier;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.api.service.ILoggerService;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.client.*;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.command.server.ConnectServerCommand;
import ru.t1.dsinetsky.tm.command.server.DisconnectServerCommand;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.service.CommandService;
import ru.t1.dsinetsky.tm.service.LoggerService;
import ru.t1.dsinetsky.tm.service.PropertyService;
import ru.t1.dsinetsky.tm.util.SystemUtil;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String COMMAND_PACKAGE = "ru.t1.dsinetsky.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ConnectionEndpointClient connectionEndpoint = new ConnectionEndpointClient();

    @Getter
    @NotNull
    private final DomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @Getter
    @NotNull
    private final AuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NotNull
    private final UserEndpointClient userEndpoint = new UserEndpointClient();

    @Getter
    @NotNull
    private final AdminEndpointClient adminEndpoint = new AdminEndpointClient();

    @Getter
    @NotNull
    private final ProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final TaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NotNull
    private final SystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        try {
            @NotNull final Reflections reflection = new Reflections(COMMAND_PACKAGE);
            @NotNull final Set<Class<? extends AbstractCommand>> classes = reflection.getSubTypesOf(AbstractCommand.class);
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
        } catch (@NotNull final InstantiationException | IllegalAccessException e) {
            loggerService.error(e);
        }
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws InstantiationException, IllegalAccessException {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry(clazz.newInstance());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void argumentRun(@Nullable final String arg) throws GeneralException {
        if (arg == null) throw new InvalidArgumentException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        abstractCommand.execute();
    }

    private boolean argRun(@NotNull final String[] args) throws GeneralException {
        if (args.length < 1) {
            return false;
        }
        @NotNull final String param = args[0];
        argumentRun(param);
        return true;
    }

    private void exitApp() {
        System.exit(0);
    }

    private void initPID() throws IOException {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String PID = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), PID.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("Welcome to the task-manager_v." + propertyService.getApplicationVersion() + "!\nType \"help\" for list of commands");
    }

    private void connect() throws GeneralException {
        terminalRun(ConnectServerCommand.NAME);
    }

    private void disconnect() throws GeneralException {
        terminalRun(DisconnectServerCommand.NAME);
    }

    private void prepareStartup() throws GeneralException, IOException {
        initLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::initShutdown));
        initPID();
        fileScanner.init();
        connect();
    }

    @SneakyThrows
    private void initShutdown() {
        loggerService.info("Thank you for using task-manager!");
        fileScanner.stop();
        disconnect();
    }

    void terminalRun(@Nullable final String command) throws GeneralException {
        if (command == null) throw new InvalidCommandException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        abstractCommand.execute();
    }

    public void run(@NotNull final String[] args) {
        try {
            if (argRun(args)) {
                exitApp();
                return;
            }
        } catch (@NotNull final GeneralException e) {
            System.err.println(e.getMessage());
            exitApp();
            return;
        }
        try {
            prepareStartup();
        } catch (GeneralException | IOException e) {
            loggerService.error(e);
        }
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                @Nullable final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                terminalRun(command);
            } catch (final @NotNull GeneralException | RuntimeException e) {
                loggerService.error(e);
            }
        }
    }

}
