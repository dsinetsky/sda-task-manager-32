package ru.t1.dsinetsky.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserChangePasswordResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateEmailResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateNameResponse;

public final class UserEndpointClient extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateEmailResponse updateUserEmail(@NotNull final UserUpdateEmailRequest request) {
        return call(request, UserUpdateEmailResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateNameResponse updateUserName(@NotNull final UserUpdateNameRequest request) {
        return call(request, UserUpdateNameResponse.class);
    }

}
