package ru.t1.dsinetsky.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public abstract class AbstractCommand implements ICommand {

    private IServiceLocator serviceLocator;

    public abstract void execute() throws GeneralException;

    @NotNull
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String desc = getDescription();
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument + ",";
        if (!name.isEmpty()) result += name + " - ";
        if (!desc.isEmpty()) result += desc;
        return result;
    }

}
