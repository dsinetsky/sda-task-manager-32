package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskListRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskListResponse;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_LIST;

    @NotNull
    public static final String DESCRIPTION = "Show available tasks";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        @Nullable final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest();
        request.setSort(sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTask(request);
        @NotNull final List<Task> listTasks = response.getTasks();
        showListTask(listTasks);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
