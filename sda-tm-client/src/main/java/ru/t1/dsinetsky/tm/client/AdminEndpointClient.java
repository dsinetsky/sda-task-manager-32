package ru.t1.dsinetsky.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserLockResponse;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserUnlockResponse;

public final class AdminEndpointClient extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUserByLogin(final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUserByLogin(final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

}
