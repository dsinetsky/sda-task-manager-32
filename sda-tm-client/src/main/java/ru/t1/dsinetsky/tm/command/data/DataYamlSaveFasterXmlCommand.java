package ru.t1.dsinetsky.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.data.DataYamlSaveFasterXmlRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_YAML_SAVE;

    @NotNull
    public static final String DESCRIPTION = "Saves data to yaml file with fasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest());
        System.out.println("File successfully saved!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
