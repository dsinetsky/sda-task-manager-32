package ru.t1.dsinetsky.tm.command.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;

public abstract class AbstractServerCommand extends AbstractCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
