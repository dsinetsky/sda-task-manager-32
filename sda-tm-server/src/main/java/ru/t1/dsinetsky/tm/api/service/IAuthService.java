package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password) throws GeneralException;

    User check(@Nullable String login, @Nullable String password) throws GeneralUserException;

    void lockUserByLogin(@Nullable String login) throws GeneralException;

    void unlockUserByLogin(@Nullable String login) throws GeneralException;
}
