package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.dto.request.task.*;
import ru.t1.dsinetsky.tm.dto.response.task.*;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService = serviceLocator.getTaskService();

    public TaskEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskFindByIdResponse findTaskById(@NotNull final TaskFindByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.findById(userId, id);
        return new TaskFindByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskFindByIndexResponse findTaskByIndex(@NotNull final TaskFindByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.findByIndex(userId, index);
        return new TaskFindByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListByProjectResponse listTaskByProject(@NotNull final TaskListByProjectRequest request) {
        check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Task> tasks = taskService.returnTasksOfProject(userId, projectId);
        return new TaskListByProjectResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Task> tasks = taskService.returnAll(userId);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        serviceLocator.getProjectTaskService().unbindProjectById(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = request.getUserId();
        serviceLocator.getProjectTaskService().bindProjectById(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIdResponse completeTaskById(final TaskCompleteByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskCompleteByIndexResponse completeTaskByIndex(final TaskCompleteByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = request.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskCompleteByIndexResponse(task);
    }

}
