package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.AccessDeniedException;
import ru.t1.dsinetsky.tm.model.User;

public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request) throws AccessDeniedException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    protected void checkRole(@Nullable final AbstractUserRequest request, @Nullable final Role role) throws GeneralException {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role userRole = user.getRole();
        final boolean check = userRole == role;
        if (!check) throw new AccessDeniedException();
    }

}
