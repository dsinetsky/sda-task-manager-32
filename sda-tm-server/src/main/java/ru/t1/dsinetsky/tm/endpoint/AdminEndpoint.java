package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dsinetsky.tm.api.service.IAuthService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserLockResponse;
import ru.t1.dsinetsky.tm.dto.response.user.admin.UserUnlockResponse;
import ru.t1.dsinetsky.tm.enumerated.Role;

public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private IAuthService authService = serviceLocator.getAuthService();

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUserByLogin(@NotNull final UserLockRequest request) {
        checkRole(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        authService.lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUserByLogin(@NotNull final UserUnlockRequest request) {
        checkRole(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        authService.lockUserByLogin(login);
        return new UserUnlockResponse();
    }

}
