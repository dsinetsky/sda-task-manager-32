package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.IDomainEndpoint;
import ru.t1.dsinetsky.tm.api.service.IDomainService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.data.*;
import ru.t1.dsinetsky.tm.dto.response.data.*;
import ru.t1.dsinetsky.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private IDomainService domainService = serviceLocator.getDomainService();

    public DomainEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataFromJsonWithFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataToJsonWithFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataFromJsonWithJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataToJsonWithJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataFromXmlWithFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataToXmlWithFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataFromXmlWithJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataToXmlWithJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.loadDataFromYamlWithFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        checkRole(request, Role.ADMIN);
        domainService.saveDataToYamlWithFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
