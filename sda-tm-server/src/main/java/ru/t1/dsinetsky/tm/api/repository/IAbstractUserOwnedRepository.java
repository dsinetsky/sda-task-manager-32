package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IAbstractUserOwnedRepository<M extends AbstractModel> extends IAbstractRepository<M> {

    @NotNull
    List<M> returnAll(@NotNull String userId) throws GeneralException;

    @NotNull
    List<M> returnAll(@NotNull String userId, @Nullable Comparator comparator) throws GeneralException;

    void clear(@NotNull String userId) throws GeneralException;

    @Nullable
    M findById(@NotNull String userId, @NotNull String id) throws GeneralException;

    @Nullable
    M findByIndex(@NotNull String userId, int index) throws GeneralException;

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id) throws GeneralException;

    @Nullable
    M removeByIndex(@Nullable String userId, int index) throws GeneralException;

    boolean existsById(@NotNull String userId, @NotNull String id) throws GeneralException;

    int getSize(@NotNull String userId) throws GeneralException;

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws GeneralException;

}
