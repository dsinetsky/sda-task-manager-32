package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserChangePasswordResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateEmailResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateNameResponse;
import ru.t1.dsinetsky.tm.exception.user.AccessDeniedException;
import ru.t1.dsinetsky.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    public UserEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = userService.changePassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = serviceLocator.getAuthService().registry(login, password);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateEmailResponse updateUserEmail(@NotNull final UserUpdateEmailRequest request) {
        check(request);
        @Nullable final String email = request.getEmail();
        @Nullable final String userId = request.getUserId();
        @NotNull final User user = userService.updateEmailById(userId, email);
        return new UserUpdateEmailResponse(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateNameResponse updateUserName(@NotNull final UserUpdateNameRequest request) {
        check(request);
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String userId = request.getUserId();
        @NotNull final User user = userService.updateUserById(userId, firstName, lastName, middleName);
        return new UserUpdateNameResponse(user);
    }

}
