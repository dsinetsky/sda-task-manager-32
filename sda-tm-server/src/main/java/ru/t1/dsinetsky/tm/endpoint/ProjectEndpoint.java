package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.project.*;
import ru.t1.dsinetsky.tm.dto.response.project.*;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService = serviceLocator.getProjectService();

    public ProjectEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectFindByIdResponse findProjectById(@NotNull final ProjectFindByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.findById(userId, id);
        return new ProjectFindByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectFindByIndexResponse findProjectByIndex(@NotNull final ProjectFindByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.findByIndex(userId, index);
        return new ProjectFindByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @NotNull final List<Project> projects = projectService.returnAll(userId);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = serviceLocator.getProjectTaskService().removeProjectById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = serviceLocator.getProjectTaskService().removeProjectByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse startProjectById(@NotNull final ProjectStartByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull final ProjectStartByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull final ProjectChangeStatusByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse completeProjectById(final ProjectCompleteByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusById(userId, id, status);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse completeProjectByIndex(final ProjectCompleteByIndexRequest request) {
        check(request);
        final int index = request.getIndex();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = request.getUserId();
        @NotNull final Project project = projectService.changeStatusByIndex(userId, index, status);
        return new ProjectCompleteByIndexResponse(project);
    }

}
