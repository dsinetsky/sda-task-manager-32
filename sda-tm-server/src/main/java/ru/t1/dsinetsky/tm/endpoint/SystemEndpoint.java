package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.system.SystemVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    private final IPropertyService propertyService = serviceLocator.getPropertyService();

    public SystemEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public SystemAboutResponse getAbout(@NotNull final SystemAboutRequest request) {
        @NotNull final SystemAboutResponse response = new SystemAboutResponse();
        response.setName(propertyService.getDeveloperName());
        response.setEmail(propertyService.getDeveloperEmail());
        return response;
    }

    @NotNull
    @Override
    public SystemVersionResponse getVersion(@NotNull final SystemVersionRequest request) {
        @NotNull final SystemVersionResponse response = new SystemVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
