package ru.t1.dsinetsky.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.*;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.*;
import ru.t1.dsinetsky.tm.dto.request.data.*;
import ru.t1.dsinetsky.tm.dto.request.project.*;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.request.task.*;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.endpoint.*;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.repository.UserRepository;
import ru.t1.dsinetsky.tm.service.*;
import ru.t1.dsinetsky.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    @Getter
    private final ITestCreateService testCreateService = new TestCreateService(projectService, taskService, userService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    @Getter
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    {
        server.registry(SystemAboutRequest.class, systemEndpoint::getAbout);
        server.registry(SystemVersionRequest.class, systemEndpoint::getVersion);

        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskByIndex);
        server.registry(TaskFindByIdRequest.class, taskEndpoint::findTaskById);
        server.registry(TaskFindByIndexRequest.class, taskEndpoint::findTaskByIndex);
        server.registry(TaskListByProjectRequest.class, taskEndpoint::listTaskByProject);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectByIndex);
        server.registry(ProjectFindByIdRequest.class, projectEndpoint::findProjectById);
        server.registry(ProjectFindByIndexRequest.class, projectEndpoint::findProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::loadDataJsonJaxB);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::saveDataJsonJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::loadDataXmlJaxB);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::saveDataXmlJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserUpdateEmailRequest.class, userEndpoint::updateUserEmail);
        server.registry(UserUpdateNameRequest.class, userEndpoint::updateUserName);
        server.registry(UserLockRequest.class, adminEndpoint::lockUserByLogin);
        server.registry(UserUnlockRequest.class, adminEndpoint::unlockUserByLogin);
    }

    private void initPID() throws IOException {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String PID = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), PID.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("Task-manager Server is up!");
    }

    private void initUser() throws GeneralException {
        testCreateService.createTest();
    }

    private void prepareStartup() throws GeneralException, IOException {
        initLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::initShutdown));
        initUser();
        initPID();
        backup.init();
        server.start();
    }

    private void initShutdown() {
        loggerService.info("Task-manager Server is down!");
        backup.stop();
        server.stop();
    }

    public void run(@NotNull final String[] args) {
        try {
            prepareStartup();
        } catch (GeneralException | IOException e) {
            loggerService.error(e);
        }
    }

}
