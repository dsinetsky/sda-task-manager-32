package ru.t1.dsinetsky.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void stop() {
        executorService.shutdown();
    }

    public void init() throws GeneralException {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 30, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() throws GeneralException {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

}
