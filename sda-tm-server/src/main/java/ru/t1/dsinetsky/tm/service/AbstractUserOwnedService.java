package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.api.service.IAbstractUserOwnedService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.EntityIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.field.IndexOutOfSizeException;
import ru.t1.dsinetsky.tm.exception.field.NegativeIndexException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;


public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IAbstractUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final String userId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        return repository.returnAll(userId);
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final String userId, @Nullable final Comparator comparator) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (comparator == null) return returnAll(userId);
        return repository.returnAll(userId, comparator);
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final String userId, @Nullable final Sort sort) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (sort == null) return returnAll(userId);
        return repository.returnAll(userId, sort.getComparator());
    }

    @Override
    @NotNull
    public M add(@Nullable final String userId, @Nullable final M model) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (model == null) throw new EntityNotFoundException(getModelType());
        return repository.add(userId, model);
    }

    @Override
    public void clear(@Nullable final String userId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        repository.clear(userId);
    }

    @Override
    @NotNull
    public M findById(@Nullable final String userId, @Nullable final String id) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        @Nullable final M model = repository.findById(userId, id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    @NotNull
    public M findByIndex(@Nullable final String userId, final int index) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize(userId)) throw new IndexOutOfSizeException(repository.getSize(userId));
        return repository.findByIndex(userId, index);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String userId, @Nullable final String id) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        @Nullable final M model = repository.removeById(userId, id);
        if (model == null) throw new EntityNotFoundException(getModelType());
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@Nullable final String userId, final int index) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= repository.getSize(userId)) throw new IndexOutOfSizeException(repository.getSize(userId));
        return repository.removeByIndex(userId, index);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new EntityIdIsEmptyException(getModelType());
        return repository.existsById(userId, id);
    }

    @Override
    public int getSize(@Nullable final String userId) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        return repository.getSize(userId);
    }

}
